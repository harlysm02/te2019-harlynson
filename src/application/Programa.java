package application;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import java.util.List;
import java.util.Scanner;

import domain.Pessoa;


public class Programa {
	
	public static EntityManagerFactory EMF = Persistence.createEntityManagerFactory("exemplo-jpa");
	public static EntityManager EM = EMF.createEntityManager();
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String email;
		String nome;
		int id;
		int value;
		
		
		do {
			printMenu();
			System.out.print("\nSua escolha: ");
			value = scan.nextInt();
			
			switch(value) {
				case 1:
					listDB();
					break;
				
				case 2:
					System.out.print("\nDigite o ID da pessoa: ");
					id = scan.nextInt();
					System.out.println(findPersonById(id));
					break;
					
				case 3:
					System.out.println("\nDigite os dados da pessoa:");
					System.out.print("Nome: ");
					nome = scan.next();
					System.out.print("Email: ");
					email = scan.next();
					
					persist(nome, email);
					break;
				
				case 4:
					System.out.println("\nDigite os dados da pessoa:");
					System.out.print("Id: ");
					id = scan.nextInt();
					System.out.print("Novo nome: ");
					nome = scan.next();
					System.out.print("Novo email: ");
					email = scan.next();
					updatePerson(id, nome, email);
					break;
				
				case 5:
					System.out.print("\nDigite o ID da pessoa: ");
					id = scan.nextInt();
					deletePerson(id);
					break;
					
				default:
					if (value != 0) {
						System.out.println("Valor fora do menu!");
					}
			
			}
		} while (value != 0);
		
		EM.close();
		EMF.close();
		scan.close();
	}
	
	//REMOVER UMA PESSOA DO BANCO DE DADOS
	public static void deletePerson(int id){
		Pessoa pessoa = findPersonById(id);
		EM.getTransaction().begin();
		EM.remove(pessoa);
		EM.getTransaction().commit();
		
	}
	
	//ATUALIZAR DADOS DE UMA PESSOA
	public static void updatePerson(int id, String nome, String email) {
		Pessoa pessoa = findPersonById(id);
		pessoa.setNome(nome);
		pessoa.setEmail(email);
		EM.getTransaction().begin();
		EM.persist(pessoa);
		EM.getTransaction().commit();
		
	}
	
	//PROCURAR PESSOA PELO ID
	public static Pessoa findPersonById (int id) {
		return EM.find(Pessoa.class, id);
	}
	
	//M�TODO DE PERSIST�NCIA
	public static void persist(String nome, String email) {
		Pessoa pessoa = new Pessoa (null, nome, email);
		EM.getTransaction().begin();
		EM.persist(pessoa);
		EM.getTransaction().commit();

	}
	
	//LISTAR PESSOAS DO BANCO DE DADOS
	public static void listDB () {
		String jpql = "SELECT p FROM Pessoa p";
		List<Pessoa> pessoas = EM.createQuery(jpql, Pessoa.class).getResultList();
		for (Pessoa p:pessoas) {
			System.out.println(p);
		}
	}
	
	//MENU
	public static void printMenu() {

		System.out.println("\n\nEscolha uma das op��es\n" +
				"1 � Listar Pessoas cadastradas\r\n" + 
				"2 � Buscar uma Pessoa pelo id\r\n" + 
				"3 � Cadastrar Pessoa\r\n" + 
				"4 � Atualizar Pessoa\r\n" + 
				"5 � Remover uma Pessoa\r\n" + 
				"0 � Sair");
	}
	
}